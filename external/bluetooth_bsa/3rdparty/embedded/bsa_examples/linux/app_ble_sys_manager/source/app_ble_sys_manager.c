/*****************************************************************************
**
**  Name:           app_ble_sys_manager.c
**
**  Description:    Bluetooth BLE Sys Manager main application
**
**  Copyright (c) 2014, Cypress Corp., All Rights Reserved.
**  Cypress Bluetooth Core. Proprietary and confidential.
**
*****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/statfs.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <assert.h>
#include <fcntl.h>
#include <stdbool.h>
#include "bsa_api.h"
#include "app_ble.h"
#include "app_thread.h"
#include "app_mutex.h"
#include "app_xml_param.h"
#include "app_utils.h"
#include "app_dm.h"
#include "app_manager.h"
#include "app_socket.h"
#include "app_ble_sys_manager.h"
#include "cJSON.h"
#include "pwmconfig.h"
#include "aes.h"

const unsigned char aes_e_key[]="0f1571c947d9e8590cb7add6af7f6798";
 
#define IP_SIZE		16
#define APP_BLE_SYS_MANAGER_UUID        0xFEBB
#define BSA_BLE_GAP_ADV_SLOW_INT           2056
#define BSA_BLE_CONNECT_EVT BTA_BLE_CONNECT_EVT
#define BTM_BLE_ADVERT_TYPE_NAME_COMPLETE 0x09

extern int ble_sys_manager_config;

static char sys_manager_device_name[256] = "3308_manager"; // This is for Adv only
static char sys_manager_mac_addr[20] = {0};

static UINT8 attr_index_notify;

#define SYS_MANAGER_SERVICE_UUID             0x15AB
#define SYS_MANAGER_CHAR_CMD_UUID            0x53DD
#define SYS_MANAGER_CHAR_NOTIFY_UUID         0x52DD
#define SYS_MANAGER_DESCRIPTOR_UUID          0x2902

#define REC_BUF_SIZE    4096

static UINT16 sys_manager_characteristic_notify_characteristic_client_configuration = 0;
static UINT8 sys_manager_char_cmd_value[APP_BLE_SYS_MANAGER_GATT_ATTRIBUTE_SIZE];
static UINT8 sys_manager_char_notify_value[20] = {0x66,0x67,0x57,0x89,0x12,0x33,0x11,0xaa,0xbb,0x34,0x78}; // 0 means not configured , 1 means configured
static tAPP_BLE_SYS_MANAGER_CB app_ble_sys_manager_cb;

static char rec_buffer[REC_BUF_SIZE]={0};
static UINT16 cur_index = 0;

/*
 * Local functions
 */
static int app_ble_sys_manager_register(void);
static void app_ble_sys_manager_set_advertisement_data(void);
static int app_ble_sys_manager_create_gatt_database(void);
static int app_ble_sys_manager_create_service(tBT_UUID *service_uuid,
               UINT16 num_handle);
static int app_ble_sys_manager_start_service(UINT16 service_id);
static int app_ble_sys_manager_add_char(tAPP_BLE_WIFI_INTRODUCER_ATTRIBUTE *attr);
static void app_ble_sys_manager_profile_cback(tBSA_BLE_EVT event,
                    tBSA_BLE_MSG *p_data);
static int app_ble_sys_manager_find_free_attr(void);
static int app_ble_sys_manager_find_attr_index_by_attr_id(UINT16 attr_id);
static void app_ble_sys_manager_cmd_callback(tBSA_BLE_EVT event,
                    tBSA_BLE_MSG *p_data);
static void app_ble_sys_manager_ccc_callback(tBSA_BLE_EVT event,
                    tBSA_BLE_MSG *p_data);
static int app_ble_sys_manager_send_notification_in_order(char * data,int length);
static BOOLEAN start_wpa_supplicant(char * ssid,char * pwd);
static void app_ble_sys_manager_cmd_handler(cJSON * usr);

static void app_ble_sys_manager_wifiinfo_handler(cJSON * usr);
static int app_ble_sys_manager_send_wifistatus_notification(char *cmd_state,char *wifi_wpid,int cnn_state,int net_state);
static void app_ble_sys_manager_getsystemstatus_handler(cJSON * usr);
static int app_ble_sys_manager_send_getsystemstatus_notification(tAPP_BLE_SYS_MANAGER_SYS_INFO info);
static void app_ble_sys_manager_fileinfoget_handler(cJSON * usr);
static int app_ble_sys_manager_send_fileinfoget_notification(void);
static void app_ble_sys_manager_getkey_handler(cJSON * usr);
static int app_ble_sys_manager_send_getkey_notification(void);
static void app_ble_sys_manager_keydata_handler(cJSON * usr);
static int app_ble_sys_manager_send_keydata_notification(char * result);
static int get_file_name_by_path(char *basePath,cJSON *files);
static int get_name_from_mac(void);
static int get_system_time(char *sys_time);
static int find_str_by_path(char *find,char *path,const char *split);
static int get_local_ip(const char *eth_inf, char *ip);
static int get_system_tf_free(char *disk,double *free);
static void get_pid_by_name(char *pid, char *task_name);
static int led_pwm_set(char *channel,int period,float rate);
static int get_verdor_parameter(int cmd, char *param_buf, int len);
static int cmd_system(char *result,const char* command);
static int write_led_info(char * color,int period,int rate);
static int read_led_info(char * color,int *period,int *rate);
static int aes_encrypt_cbc(uint8_t *in,int len);
static int aes_decrypt_cbc(uint8_t *in,int len);
static uint8_t hex_to_char(uint8_t temp);
/*
 * BLE common functions
 */

/*******************************************************************************
 **
 ** Function        app_ble_sys_manager_register
 **
 ** Description     Register app
 **
 ** Parameters      None
 **
 ** Returns         status: 0 if success / -1 otherwise
 **
 *******************************************************************************/
static int app_ble_sys_manager_register(void)
{
    tBSA_STATUS status;
    tBSA_BLE_SE_REGISTER ble_register_param;

    status = BSA_BleSeAppRegisterInit(&ble_register_param);
    if (status != BSA_SUCCESS)
    {
        APP_ERROR1("BSA_BleSeAppRegisterInit failed status = %d", status);
        return -1;
    }

    ble_register_param.uuid.len = LEN_UUID_16;
    ble_register_param.uuid.uu.uuid16 = APP_BLE_SYS_MANAGER_UUID;
    ble_register_param.p_cback = app_ble_sys_manager_profile_cback;

    status = BSA_BleSeAppRegister(&ble_register_param);
    if (status != BSA_SUCCESS)
    {
        APP_ERROR1("BSA_BleSeAppRegister failed status = %d", status);
        return -1;
    }
    app_ble_sys_manager_cb.server_if = ble_register_param.server_if;
    APP_INFO1("server_if:%d", app_ble_sys_manager_cb.server_if);
    return 0;
}

/************************************* ******************************************
 **
 ** Function        app_ble_sys_manager_set_advertisement_data
 **
 ** Description     Setup advertisement data with 16 byte UUID and device name
 **
 ** Parameters      None
 **
 ** Returns         status: 0 if success / -1 otherwise
 **
 *******************************************************************************/
static void app_ble_sys_manager_set_advertisement_data(void)
{
    tBSA_DM_BLE_AD_MASK data_mask;
    tBSA_DM_SET_CONFIG bt_config;
    tBSA_STATUS bsa_status;
    UINT8 len = 0;

    //get_name_from_mac();
    get_verdor_parameter(VENDOR_SN_ID,sys_manager_device_name,20);
    
    /* Set Bluetooth configuration */
    BSA_DmSetConfigInit(&bt_config);

    /* Obviously */
    bt_config.enable = TRUE;

    /* Configure the Advertisement Data parameters */
    bt_config.config_mask = BSA_DM_CONFIG_BLE_ADV_CONFIG_MASK;

    /* Use services flag to show above services if required on the peer device */
    data_mask = BSA_DM_BLE_AD_BIT_FLAGS | BSA_DM_BLE_AD_BIT_PROPRIETARY |
                         BSA_DM_BLE_AD_BIT_SERVICE;

    bt_config.adv_config.flag = BSA_DM_BLE_GEN_DISC_FLAG | BSA_DM_BLE_BREDR_NOT_SPT;
    bt_config.adv_config.adv_data_mask = data_mask;
    bt_config.adv_config.is_scan_rsp = FALSE;

    bt_config.adv_config.num_service = 1;
    bt_config.adv_config.uuid_val[0] = SYS_MANAGER_SERVICE_UUID;
    len += bt_config.adv_config.num_service * sizeof(UINT16);

    bt_config.adv_config.proprietary.num_elem = 1;
    bt_config.adv_config.proprietary.elem[0].adv_type = BTM_BLE_ADVERT_TYPE_NAME_COMPLETE;
    len += 2;

    bt_config.adv_config.proprietary.elem[0].len = strlen((char *)sys_manager_device_name);
    strcpy((char *)bt_config.adv_config.proprietary.elem[0].val, (char *)sys_manager_device_name);
    len += bt_config.adv_config.proprietary.elem[0].len;
    bt_config.adv_config.len = len;

    bsa_status = BSA_DmSetConfig(&bt_config);
    if (bsa_status != BSA_SUCCESS)
    {
        APP_ERROR1("BSA_DmSetConfig failed status:%d ", bsa_status);
        return;
    }
    APP_INFO0("app_ble_sys_manager_set_advertisement_data");
}

/*******************************************************************************
 **
 ** Function        app_ble_sys_manager_create_gatt_database
 **
 ** Description     This is the GATT database for the WiFi Introducer Sensor application.
 **                       It defines services, characteristics and descriptors supported by the sensor.
 **
 ** Parameters      None
 **
 ** Returns         status: 0 if success / -1 otherwise
 **
 *******************************************************************************/
static int app_ble_sys_manager_create_gatt_database(void)
{
    int srvc_attr_index;
    tBT_UUID service_uuid;
    tAPP_BLE_WIFI_INTRODUCER_ATTRIBUTE attr;

    APP_INFO0("app_ble_sys_manager_create_gatt_database");

    service_uuid.len = LEN_UUID_16;
    service_uuid.uu.uuid16 = SYS_MANAGER_SERVICE_UUID;
    srvc_attr_index = app_ble_sys_manager_create_service(&service_uuid, 20);
    if (srvc_attr_index < 0) {
        APP_ERROR0("Wifi Config Service Create Fail");
        return -1;
    }

    /* Declare characteristic used to notify/indicate change */
    memset(&attr, 0, sizeof(tAPP_BLE_WIFI_INTRODUCER_ATTRIBUTE));
    attr.attr_UUID.len = LEN_UUID_16;
    attr.attr_UUID.uu.uuid16 = SYS_MANAGER_CHAR_NOTIFY_UUID;
    attr.service_id = app_ble_sys_manager_cb.attr[srvc_attr_index].service_id;
    attr.perm = BSA_GATT_PERM_READ;
    attr.prop = BSA_GATT_CHAR_PROP_BIT_READ | BSA_GATT_CHAR_PROP_BIT_WRITE |
                    BSA_GATT_CHAR_PROP_BIT_NOTIFY | BSA_GATT_CHAR_PROP_BIT_INDICATE |
                    BSA_GATT_CHAR_PROP_BIT_WRITE_NR;
    attr.attr_type = BSA_GATTC_ATTR_TYPE_CHAR;
    attr.val_len = attr.max_val_len = sizeof(sys_manager_char_notify_value);
    attr.p_val = &sys_manager_char_notify_value;
    attr_index_notify = app_ble_sys_manager_add_char(&attr);

     /* Declare client characteristic configuration descriptor
       * Value of the descriptor can be modified by the client
       * Value modified shall be retained during connection and across connection
       * for bonded devices.  Setting value to 1 tells this application to send notification
       * when value of the characteristic changes.  Value 2 is to allow indications. */
    memset(&attr, 0, sizeof(tAPP_BLE_WIFI_INTRODUCER_ATTRIBUTE));
    attr.attr_UUID.len = LEN_UUID_16;
    attr.attr_UUID.uu.uuid16 = SYS_MANAGER_DESCRIPTOR_UUID;
    attr.service_id = app_ble_sys_manager_cb.attr[srvc_attr_index].service_id;
    attr.perm = BSA_GATT_PERM_READ | BSA_GATT_PERM_WRITE; //17
    attr.prop = BSA_GATT_CHAR_PROP_BIT_READ | BSA_GATT_CHAR_PROP_BIT_WRITE |
                BSA_GATT_CHAR_PROP_BIT_NOTIFY | BSA_GATT_CHAR_PROP_BIT_INDICATE |
                BSA_GATT_CHAR_PROP_BIT_WRITE_NR;
    attr.attr_type = BSA_GATTC_ATTR_TYPE_CHAR_DESCR;
    attr.val_len = attr.max_val_len = sizeof(UINT16);
    attr.p_val = &sys_manager_characteristic_notify_characteristic_client_configuration;
    attr.p_cback = app_ble_sys_manager_ccc_callback;
    app_ble_sys_manager_add_char(&attr);

    memset(&attr, 0, sizeof(tAPP_BLE_WIFI_INTRODUCER_ATTRIBUTE));
    attr.attr_UUID.len = LEN_UUID_16;
    attr.attr_UUID.uu.uuid16 = SYS_MANAGER_CHAR_CMD_UUID;
    attr.service_id = app_ble_sys_manager_cb.attr[srvc_attr_index].service_id;
    attr.perm = BSA_GATT_PERM_READ | BSA_GATT_PERM_WRITE; //17
    attr.prop = BSA_GATT_CHAR_PROP_BIT_READ | BSA_GATT_CHAR_PROP_BIT_WRITE |
                    BSA_GATT_CHAR_PROP_BIT_NOTIFY | BSA_GATT_CHAR_PROP_BIT_INDICATE |
                    BSA_GATT_CHAR_PROP_BIT_WRITE_NR;
    attr.attr_type = BSA_GATTC_ATTR_TYPE_CHAR;
    attr.val_len = strlen(CLIENT_AP_PASSPHRASE);
    attr.max_val_len = APP_BLE_SYS_MANAGER_GATT_ATTRIBUTE_SIZE - 1; // reserve one byte for end of string
    attr.p_val = sys_manager_char_cmd_value;
    attr.p_cback = app_ble_sys_manager_cmd_callback;
    app_ble_sys_manager_add_char(&attr);

    return 0;
}

/*******************************************************************************
 **
 ** Function        app_ble_sys_manager_create_service
 **
 ** Description     create service
 **
 ** Parameters     service UUID
 **                       number of handle for reserved
 **
 ** Returns         status: 0 if success / -1 otherwise
 **
 *******************************************************************************/
static int app_ble_sys_manager_create_service(tBT_UUID *service_uuid,
               UINT16 num_handle)
{
    tBSA_STATUS status;
    tBSA_BLE_SE_CREATE ble_create_param;
    int attr_index = -1;
    UINT32 timeout = APP_BLE_SYS_MANAGER_TIMEOUT;

    attr_index = app_ble_sys_manager_find_free_attr();
    if (attr_index < 0)
    {
        APP_ERROR1("Wrong attr number! = %d", attr_index);
        return -1;
    }

    status = BSA_BleSeCreateServiceInit(&ble_create_param);
    if (status != BSA_SUCCESS)
    {
        APP_ERROR1("BSA_BleSeCreateServiceInit failed status = %d", status);
        return -1;
    }

    memcpy(&ble_create_param.service_uuid, service_uuid, sizeof(tBT_UUID));
    ble_create_param.server_if = app_ble_sys_manager_cb.server_if;
    ble_create_param.num_handle = num_handle;
    ble_create_param.is_primary = TRUE;

    app_ble_sys_manager_cb.attr[attr_index].wait_flag = TRUE;

    if ((status = BSA_BleSeCreateService(&ble_create_param)) == BSA_SUCCESS)
    {
        while (app_ble_sys_manager_cb.attr[attr_index].wait_flag && timeout)
        {
            GKI_delay(100);
            timeout--;
        }
    }
    if ((status != BSA_SUCCESS) || (timeout == 0))
    {
        APP_ERROR1("BSA_BleSeCreateService failed status = %d", status);
        app_ble_sys_manager_cb.attr[attr_index].wait_flag = FALSE;
        return -1;
    }

    /* store information on control block */
    memcpy(&app_ble_sys_manager_cb.attr[attr_index].attr_UUID, service_uuid,
                    sizeof(tBT_UUID));
    app_ble_sys_manager_cb.attr[attr_index].is_pri = ble_create_param.is_primary;
    app_ble_sys_manager_cb.attr[attr_index].attr_type = BSA_GATTC_ATTR_TYPE_SRVC;
    return attr_index;
}

/*******************************************************************************
 **
 ** Function        app_ble_sys_manager_start_service
 **
 ** Description     Start Service
 **
 ** Parameters      service_id : attr id
 **
 ** Returns         status: 0 if success / -1 otherwise
 **
 *******************************************************************************/
static int app_ble_sys_manager_start_service(UINT16 service_id)
{
    tBSA_STATUS status;
    tBSA_BLE_SE_START ble_start_param;

    status = BSA_BleSeStartServiceInit(&ble_start_param);
    if (status != BSA_SUCCESS)
    {
        APP_ERROR1("BSA_BleSeStartServiceInit failed status = %d", status);
        return -1;
    }

    ble_start_param.service_id = service_id;
    ble_start_param.sup_transport = BSA_BLE_GATT_TRANSPORT_LE;

    APP_INFO1("service_id:%d", ble_start_param.service_id);

    status = BSA_BleSeStartService(&ble_start_param);
    if (status != BSA_SUCCESS)
    {
        APP_ERROR1("BSA_BleSeStartService failed status = %d", status);
        return -1;
    }
    return 0;
}

/*******************************************************************************
 **
 ** Function        app_ble_sys_manager_add_char
 **
 ** Description     Add character to service
 **
 ** Parameters      tAPP_BLE_WIFI_INTRODUCER_ATTRIBUTE
 **
 ** Returns         status: 0 if success / -1 otherwise
 **
 *******************************************************************************/
static int app_ble_sys_manager_add_char(tAPP_BLE_WIFI_INTRODUCER_ATTRIBUTE *attr)
{
    tBSA_STATUS status;
    tBSA_BLE_SE_ADDCHAR ble_addchar_param;
    int attr_index = -1;
    UINT32 timeout = APP_BLE_SYS_MANAGER_TIMEOUT;

    attr_index = app_ble_sys_manager_find_free_attr();
    if (attr_index < 0)
    {
        APP_ERROR1("Wrong attr index! = %d", attr_index);
        return -1;
    }

    status = BSA_BleSeAddCharInit(&ble_addchar_param);
    if (status != BSA_SUCCESS)
    {
        APP_ERROR1("BSA_BleSeAddCharInit failed status = %d", status);
        return -1;
    }

    /* characteristic */
    ble_addchar_param.service_id = attr->service_id;
    ble_addchar_param.is_descr = (attr->attr_type == BSA_GATTC_ATTR_TYPE_CHAR_DESCR);
    memcpy(&ble_addchar_param.char_uuid, &attr->attr_UUID, sizeof(tBT_UUID));
    ble_addchar_param.perm = attr->perm;
    ble_addchar_param.property = attr->prop;

    APP_INFO1("app_ble_sys_manager_add_char service_id:%d", ble_addchar_param.service_id);
    app_ble_sys_manager_cb.attr[attr_index].wait_flag = TRUE;

    if ((status = BSA_BleSeAddChar(&ble_addchar_param)) == BSA_SUCCESS)
    {
        while (app_ble_sys_manager_cb.attr[attr_index].wait_flag && timeout)
        {
            GKI_delay(100);
            timeout--;
        }
    }
    if ((status != BSA_SUCCESS) || (timeout == 0))
    {
        APP_ERROR1("BSA_BleSeAddChar failed status = %d", status);
        return -1;
    }
	
    /* store information on control block */
    memcpy(&app_ble_sys_manager_cb.attr[attr_index].attr_UUID, &attr->attr_UUID,
                    sizeof(tBT_UUID));
    app_ble_sys_manager_cb.attr[attr_index].attr_type = attr->attr_type;
    app_ble_sys_manager_cb.attr[attr_index].val_len = attr->val_len;
    app_ble_sys_manager_cb.attr[attr_index].max_val_len = attr->max_val_len;
    app_ble_sys_manager_cb.attr[attr_index].p_val = attr->p_val;
    app_ble_sys_manager_cb.attr[attr_index].p_cback = attr->p_cback;
    return attr_index;
}

/*******************************************************************************
**
** Function         app_ble_sys_manager_profile_cback
**
** Description      APP BLE wifi introducer Profile callback.
**
** Returns          void
**
*******************************************************************************/
static void app_ble_sys_manager_profile_cback(tBSA_BLE_EVT event,
                   tBSA_BLE_MSG *p_data)
{
    int attr_index;
    tBSA_BLE_SE_SENDRSP send_server_resp;
    tBSA_DM_BLE_ADV_PARAM adv_param;
    int attr_len_to_copy;

    APP_DEBUG1("event = %d ", event);

    switch (event)
    {
    case BSA_BLE_SE_DEREGISTER_EVT:
        APP_INFO1("BSA_BLE_SE_DEREGISTER_EVT server_if:%d status:%d",
            p_data->ser_deregister.server_if, p_data->ser_deregister.status);
        if (p_data->ser_deregister.server_if != app_ble_sys_manager_cb.server_if)
        {
            APP_ERROR0("wrong deregister interface!!");
            break;
        }

        app_ble_sys_manager_cb.server_if = BSA_BLE_INVALID_IF;
        for (attr_index = 0; attr_index < APP_BLE_WIFI_INTRODUCER_ATTRIBUTE_MAX; attr_index++)
        {
            memset(&app_ble_sys_manager_cb.attr[attr_index], 0,
                              sizeof(tAPP_BLE_WIFI_INTRODUCER_ATTRIBUTE));
        }
        break;

    case BSA_BLE_SE_CREATE_EVT:
        APP_INFO1("BSA_BLE_SE_CREATE_EVT server_if:%d status:%d service_id:%d",
            p_data->ser_create.server_if, p_data->ser_create.status, p_data->ser_create.service_id);

        /* search interface number */
        if (p_data->ser_create.server_if != app_ble_sys_manager_cb.server_if)
        {
            APP_ERROR0("interface wrong!!");
            break;
        }

        /* search attribute number */
        for (attr_index = 0; attr_index < APP_BLE_WIFI_INTRODUCER_ATTRIBUTE_MAX; attr_index++)
        {
            if (app_ble_sys_manager_cb.attr[attr_index].wait_flag == TRUE)
            {
                APP_INFO1("BSA_BLE_SE_CREATE_EVT attr_index:%d", attr_index);
                if (p_data->ser_create.status == BSA_SUCCESS)
                {
                    app_ble_sys_manager_cb.attr[attr_index].service_id = p_data->ser_create.service_id;
                    app_ble_sys_manager_cb.attr[attr_index].attr_id = p_data->ser_create.service_id;
                    app_ble_sys_manager_cb.attr[attr_index].wait_flag = FALSE;
                    break;
                }
                else  /* if CREATE fail */
                {
                    memset(&app_ble_sys_manager_cb.attr[attr_index], 0, sizeof(tAPP_BLE_WIFI_INTRODUCER_ATTRIBUTE));
                    break;
                }
            }
        }
        if (attr_index >= APP_BLE_WIFI_INTRODUCER_ATTRIBUTE_MAX)
        {
            APP_ERROR0("BSA_BLE_SE_CREATE_EVT no waiting!!");
            break;
        }
        break;

    case BSA_BLE_SE_ADDCHAR_EVT:
        APP_INFO1("BSA_BLE_SE_ADDCHAR_EVT status:%d", p_data->ser_addchar.status);
        APP_INFO1("attr_id:0x%x", p_data->ser_addchar.attr_id);

        /* search attribute number */
        for (attr_index = 0; attr_index < APP_BLE_WIFI_INTRODUCER_ATTRIBUTE_MAX; attr_index++)
        {
            if (app_ble_sys_manager_cb.attr[attr_index].wait_flag == TRUE)
            {
                APP_INFO1("BSA_BLE_SE_ADDCHAR_EVT attr_index:%d", attr_index);
                if (p_data->ser_addchar.status == BSA_SUCCESS)
                {
                    app_ble_sys_manager_cb.attr[attr_index].service_id = p_data->ser_addchar.service_id;
                    app_ble_sys_manager_cb.attr[attr_index].attr_id = p_data->ser_addchar.attr_id;
                    app_ble_sys_manager_cb.attr[attr_index].wait_flag = FALSE;
                    break;
                }
                else  /* if ADD fail */
                {
                    memset(&app_ble_sys_manager_cb.attr[attr_index], 0, sizeof(tAPP_BLE_WIFI_INTRODUCER_ATTRIBUTE));
                    break;
                }
            }
        }
        if (attr_index >= APP_BLE_WIFI_INTRODUCER_ATTRIBUTE_MAX)
        {
            APP_ERROR0("BSA_BLE_SE_ADDCHAR_EVT no waiting!!");
            break;
        }
        break;

    case BSA_BLE_SE_START_EVT:
        APP_INFO1("BSA_BLE_SE_START_EVT status:%d", p_data->ser_start.status);
        break;

    case BSA_BLE_SE_STOP_EVT:
        APP_INFO1("BSA_BLE_SE_STOP_EVT status:%d", p_data->ser_stop.status);
        break;

    case BSA_BLE_SE_READ_EVT:
        APP_INFO1("BSA_BLE_SE_READ_EVT status:%d, handle:%d offset:%d", p_data->ser_read.status, p_data->ser_read.handle, p_data->ser_read.offset);
        BSA_BleSeSendRspInit(&send_server_resp);
        send_server_resp.conn_id = p_data->ser_read.conn_id;
        send_server_resp.trans_id = p_data->ser_read.trans_id;
        send_server_resp.status = p_data->ser_read.status;
        send_server_resp.handle = p_data->ser_read.handle;
        send_server_resp.offset = p_data->ser_read.offset;
	    attr_index = app_ble_sys_manager_find_attr_index_by_attr_id(p_data->ser_read.handle);
	    APP_INFO1("BSA_BLE_SE_READ_EVT attr_index:%d", attr_index);
        if (attr_index < 0)
        {
            APP_ERROR0("Cannot find matched attr_id");
	     break;
        }
        attr_len_to_copy = app_ble_sys_manager_cb.attr[attr_index].val_len;
        if (p_data->ser_read.offset >= attr_len_to_copy)
            attr_len_to_copy = send_server_resp.len = 0;
        if (attr_len_to_copy != 0)
        {
            attr_len_to_copy -= p_data->ser_read.offset;
        }
        send_server_resp.len = attr_len_to_copy;
        send_server_resp.auth_req = GATT_AUTH_REQ_NONE;
        memcpy(send_server_resp.value, app_ble_sys_manager_cb.attr[attr_index].p_val + p_data->ser_read.offset,
                      send_server_resp.len);
        APP_INFO1("BSA_BLE_SE_READ_EVT: send_server_resp.conn_id:%d, send_server_resp.trans_id:%d", send_server_resp.conn_id, send_server_resp.trans_id, send_server_resp.status);
        APP_INFO1("BSA_BLE_SE_READ_EVT: send_server_resp.status:%d,send_server_resp.auth_req:%d", send_server_resp.status,send_server_resp.auth_req);
        APP_INFO1("BSA_BLE_SE_READ_EVT: send_server_resp.handle:%d, send_server_resp.offset:%d, send_server_resp.len:%d", send_server_resp.handle,send_server_resp.offset,send_server_resp.len );
        BSA_BleSeSendRsp(&send_server_resp);
	 if (app_ble_sys_manager_cb.attr[attr_index].p_cback)
           app_ble_sys_manager_cb.attr[attr_index].p_cback(event, p_data);
        break;

    case BSA_BLE_SE_WRITE_EVT:
        APP_INFO1("BSA_BLE_SE_WRITE_EVT status:%d", p_data->ser_write.status);
        APP_DUMP("Write value", p_data->ser_write.value, p_data->ser_write.len);
        APP_INFO1("BSA_BLE_SE_WRITE_EVT trans_id:%d, conn_id:%d, handle:%d",
            p_data->ser_write.trans_id, p_data->ser_write.conn_id, p_data->ser_write.handle);

	    attr_index = app_ble_sys_manager_find_attr_index_by_attr_id(p_data->ser_read.handle);
        APP_INFO1("BSA_BLE_SE_WRITE_EVT attr_index:%d", attr_index);
        if (attr_index < 0)
        {
            APP_ERROR0("Cannot find matched attr_id");
	        break;
        }
        if (p_data->ser_write.len >= app_ble_sys_manager_cb.attr[attr_index].max_val_len)
            p_data->ser_write.len = app_ble_sys_manager_cb.attr[attr_index].max_val_len;
        app_ble_sys_manager_cb.attr[attr_index].val_len = p_data->ser_write.len;
        memcpy(app_ble_sys_manager_cb.attr[attr_index].p_val, p_data->ser_write.value,
                       p_data->ser_write.len);
        if (p_data->ser_write.need_rsp)
        {
            BSA_BleSeSendRspInit(&send_server_resp);
            send_server_resp.conn_id = p_data->ser_write.conn_id;
            send_server_resp.trans_id = p_data->ser_write.trans_id;
            send_server_resp.status = p_data->ser_write.status;
            send_server_resp.handle = p_data->ser_write.handle;
            send_server_resp.len = 0;
            APP_INFO1("BSA_BLE_SE_WRITE_EVT: send_server_resp.conn_id:%d, send_server_resp.trans_id:%d", send_server_resp.conn_id, send_server_resp.trans_id, send_server_resp.status);
            APP_INFO1("BSA_BLE_SE_WRITE_EVT: send_server_resp.status:%d,send_server_resp.auth_req:%d", send_server_resp.status,send_server_resp.auth_req);
            APP_INFO1("BSA_BLE_SE_WRITE_EVT: send_server_resp.handle:%d, send_server_resp.offset:%d, send_server_resp.len:%d", send_server_resp.handle,send_server_resp.offset,send_server_resp.len );
            BSA_BleSeSendRsp(&send_server_resp);
        }
	 if (app_ble_sys_manager_cb.attr[attr_index].p_cback)
           app_ble_sys_manager_cb.attr[attr_index].p_cback(event, p_data);
        break;

    case BSA_BLE_SE_EXEC_WRITE_EVT:
        APP_INFO1("BSA_BLE_SE_EXEC_WRITE_EVT status:%d", p_data->ser_exec_write.status);
        APP_INFO1("BSA_BLE_SE_EXEC_WRITE_EVT trans_id:%d, conn_id:%d, flag:%d",
            p_data->ser_exec_write.trans_id, p_data->ser_exec_write.conn_id,
            p_data->ser_exec_write.flag);

        break;


    case BSA_BLE_SE_OPEN_EVT:
        APP_INFO1("BSA_BLE_SE_OPEN_EVT status:%d", p_data->ser_open.reason);
        if (p_data->ser_open.reason == BSA_SUCCESS)
        {
            APP_INFO1("app_ble_sys_manager_conn_up conn_id:0x%x", p_data->ser_open.conn_id);
            app_ble_sys_manager_cb.conn_id = p_data->ser_open.conn_id;

            APP_INFO1("app_ble_sys_manager_conn_up connected to [%02X:%02X:%02X:%02X:%02X:%02X]",
                          p_data->ser_open.remote_bda[0],
                          p_data->ser_open.remote_bda[1],
                          p_data->ser_open.remote_bda[2],
                          p_data->ser_open.remote_bda[3],
                          p_data->ser_open.remote_bda[4],
                          p_data->ser_open.remote_bda[5]);

            /* Stop advertising */
            app_dm_set_ble_visibility(FALSE, FALSE);
            APP_INFO0("Stopping Advertisements");
            sys_manager_characteristic_notify_characteristic_client_configuration = 0;
            //设置蓝色指示灯常亮
            //led_set_color("B",1000,100);
        }
        break;

    case BSA_BLE_SE_CONGEST_EVT:
        APP_INFO1("BSA_BLE_SE_CONGEST_EVT  :conn_id:0x%x, congested:%d",
            p_data->ser_congest.conn_id, p_data->ser_congest.congested);
        break;

    case BSA_BLE_SE_CLOSE_EVT:
        APP_INFO1("BSA_BLE_SE_CLOSE_EVT status:%d", p_data->ser_close.reason);
        APP_INFO1("conn_id:0x%x", p_data->ser_close.conn_id);
        APP_INFO1("app_ble_wifi_introducer_connection_down  conn_id:%d reason:%d", p_data->ser_close.conn_id, p_data->ser_close.reason);

        ble_sys_manager_config = 0;

        app_ble_sys_manager_cb.conn_id = BSA_BLE_INVALID_CONN;

        /* start low advertisements */
        /* Set ADV params */
        memset(&adv_param, 0, sizeof(tBSA_DM_BLE_ADV_PARAM));
        adv_param.adv_type = BSA_BLE_CONNECT_EVT;
        adv_param.adv_int_min = BSA_BLE_GAP_ADV_SLOW_INT;
        adv_param.adv_int_max = BSA_BLE_GAP_ADV_SLOW_INT;
        app_dm_set_ble_adv_param(&adv_param);
        /* Set visisble and connectable */
        app_dm_set_ble_visibility(TRUE, TRUE);
        break;

    case BSA_BLE_SE_CONFIRM_EVT:
        APP_INFO1("BSA_BLE_SE_CONFIRM_EVT  :conn_id:0x%x, status:%d",
            p_data->ser_confirm.conn_id, p_data->ser_confirm.status);
        break;

    default:
        break;
    }
}

/*******************************************************************************
 **
 ** Function         app_ble_sys_manager_init
 **
 ** Description     APP BLE wifi introducer control block init
 **
 ** Parameters     None
 **
 ** Returns          None
 **
 *******************************************************************************/
void app_ble_sys_manager_init(void)
{
    memset(&app_ble_sys_manager_cb, 0, sizeof(app_ble_sys_manager_cb));
    app_ble_sys_manager_cb.conn_id = BSA_BLE_INVALID_CONN;
}

/*******************************************************************************
 **
 ** Function         app_ble_sys_manager_gatt_server_init
 **
 ** Description     APP BLE wifi introducer GATT Server init
 **
 ** Parameters     None
 **
 ** Returns          None
 **
 *******************************************************************************/
void app_ble_sys_manager_gatt_server_init(void)
{
    int index;

    APP_INFO0("wifi_introducer_gatt_server_init");
    /* register BLE server app */
    /* Register with stack to receive GATT callback */
    app_ble_sys_manager_register();
    GKI_delay(1000);

    app_ble_sys_manager_create_gatt_database();

    /* start service */
    for (index = 0; index < APP_BLE_WIFI_INTRODUCER_ATTRIBUTE_MAX; index++)
    {
        if (app_ble_sys_manager_cb.attr[index].attr_type == BSA_GATTC_ATTR_TYPE_SRVC)
        {
            app_ble_sys_manager_start_service(app_ble_sys_manager_cb.attr[index].service_id);
        }
    }

    GKI_delay(1000);

    /* Set the advertising parameters */
    app_ble_sys_manager_set_advertisement_data();

    /* Set visisble and connectable */
    app_dm_set_visibility(FALSE, FALSE);
    app_dm_set_ble_visibility(TRUE, TRUE);
    printf("\n");
    /*uint8_t str_enc[]  = "dirfjiki889978yuefikjudefsligjciejkuifhjfdexfuhy78543d8906juefd";
    uint8_t str_dec[64]  = {0};
    uint8_t send_str_enc[130] = {0};
    aes_encrypt_cbc(str_enc,sizeof(str_enc));
    send_str_enc[0]='"';
    int k = 0,j = 0;
    for(k=0;k<sizeof(str_enc);k++){
        send_str_enc[j] = hex_to_char((str_enc[k]&0xff) >> 4);
        j++;
        send_str_enc[j] = hex_to_char(str_enc[k]&0x0f);
        j++;
        printf("0x%x ",str_enc[k]);
        //printf("%c%c ",send_str_enc[j-2],send_str_enc[j-1]);
    }
    send_str_enc[129]='"';
    printf("\n");
    printf("enc string:%s\n",send_str_enc);
    memcpy(str_dec,str_enc,sizeof(str_enc));
    printf("size:%d\n",sizeof(str_enc));
    aes_decrypt_cbc(str_dec,sizeof(str_enc));
    printf("str_dec:%s\n",str_dec);
    printf("\n");*/
}

/*******************************************************************************
 **
 ** Function         app_ble_sys_manager_find_free_attr
 **
 ** Description      find free attr for APP BLE wifi introducer application
 **
 ** Parameters
 **
 ** Returns          positive number(include 0) if successful, error code otherwise
 **
 *******************************************************************************/
static int app_ble_sys_manager_find_free_attr(void)
{
    int index;

    for (index = 0; index < APP_BLE_WIFI_INTRODUCER_ATTRIBUTE_MAX; index++)
    {
        if (!app_ble_sys_manager_cb.attr[index].attr_UUID.uu.uuid16)
        {
            return index;
        }
    }
    return -1;
}

/*******************************************************************************
 **
 ** Function         app_ble_sys_manager_find_attr_index_by_attr_id
 **
 ** Description      find free attr for APP BLE wifi introducer application
 **
 ** Parameters     attr_id
 **
 ** Returns          positive number(include 0) if successful, error code otherwise
 **
 *******************************************************************************/
static int app_ble_sys_manager_find_attr_index_by_attr_id(UINT16 attr_id)
{
    int index;

    for (index = 0; index < APP_BLE_WIFI_INTRODUCER_ATTRIBUTE_MAX; index++)
    {
        if (app_ble_sys_manager_cb.attr[index].attr_id == attr_id)
        {
            return index;
        }
    }
    return -1;
}

/*******************************************************************************
**
** Function         app_ble_sys_manager_cmd_callback
**
** Description      APP BLE wifi introducer Password callback.
**
** Returns          void
**
*******************************************************************************/
static void app_ble_sys_manager_cmd_callback(tBSA_BLE_EVT event,
                  tBSA_BLE_MSG *p_data)
{
    int attr_index;

    APP_INFO0("app_ble_sys_manager_cmd_callback");
    if (event == BSA_BLE_SE_WRITE_EVT)
    {
	 attr_index = app_ble_sys_manager_find_attr_index_by_attr_id(p_data->ser_write.handle);
        if (attr_index < 0)
        {
            APP_ERROR0("Cannot find matched attr_id");
	        return;
        }
        if (app_ble_sys_manager_cb.attr[attr_index].val_len >= APP_BLE_SYS_MANAGER_GATT_ATTRIBUTE_SIZE)
        {
             APP_ERROR0("Wrong sys_manager_char_cmd_value length");
	     return;
        }
        int value_length = app_ble_sys_manager_cb.attr[attr_index].val_len;
        sys_manager_char_cmd_value[app_ble_sys_manager_cb.attr[attr_index].val_len] = 0;
        APP_INFO1("write passphrase value: %s", sys_manager_char_cmd_value);
        //APP_DUMP("write 1 call back", app_ble_sys_manager_cb.attr[attr_index].p_val, app_ble_sys_manager_cb.attr[attr_index].val_len);
        char fflag = sys_manager_char_cmd_value[0];
        printf("serial number 0x%x\n",fflag);
        int kk = 0;
        for(kk=0;kk<value_length;kk++){
            printf("0x%x ",sys_manager_char_cmd_value[kk]);
            if(kk >= 2){
                rec_buffer[cur_index] = sys_manager_char_cmd_value[kk];
                cur_index++;
            }
        }
		printf("app_ble_sys_manager_cmd_callback() rec_buffer%x\n ",rec_buffer);
        if(fflag == 0xff){
            cJSON * usr;
            usr=cJSON_Parse(rec_buffer);
            app_ble_sys_manager_cmd_handler(usr);
            cur_index = 0;
            memset(rec_buffer,0,REC_BUF_SIZE);
            cJSON_Delete(usr); 
        }

    }
}

/*******************************************************************************
**
** Function         app_ble_sys_manager_cmd_handler
**
** Description      command handle
**
** Returns          void
**
*******************************************************************************/
static void app_ble_sys_manager_cmd_handler(cJSON * usr)
{
    char *out = cJSON_Print(usr);
    printf("app_ble_sys_manager_cmd_handler() %s\n",out);
    cJSON *type = cJSON_GetObjectItem(usr,"cmdtype");
    char *cmd = cJSON_Print(type);
    if(type != NULL){
        printf("%s\n",cmd);
        
        if (strcmp(cmd, "\"wifiinfo\"") == 0){
            app_ble_sys_manager_wifiinfo_handler(usr);
        }else if(strcmp(cmd, "\"getsystemstatus\"") == 0){
            app_ble_sys_manager_getsystemstatus_handler(usr);
        }else if(strcmp(cmd, "\"fileinfoget\"") == 0){
            app_ble_sys_manager_fileinfoget_handler(usr);
        }else if(strcmp(cmd, "\"getkey\"") == 0){
            app_ble_sys_manager_getkey_handler(usr);
        }else if(strcmp(cmd, "\"keydata\"") == 0){
            app_ble_sys_manager_keydata_handler(usr);
        }else{
            printf("app_ble_sys_manager_cmd_handler() error unknown cmd type:%s\n",cmd);
        }
    }else{
        printf("app_ble_sys_manager_cmd_handler() error cmd type is null!\n");
    }
    free(out);
}

/*******************************************************************************
**
** Function         start_wpa_supplicant
**
** Description      wpa supplicant
**
** Returns          BOOLEAN
**
*******************************************************************************/
static BOOLEAN start_wpa_supplicant(char * ssid,char * pwd)
{
    FILE *fp = NULL;
    if ((fp = fopen("/data/cfg/wpa_supplicant.conf", "w+")) == NULL)
    {
        APP_ERROR0("open bsa wpa_supplicant.conf failed");
        return FALSE;
    }

    fprintf(fp, "%s\n", "ctrl_interface=/var/run/wpa_supplicant");
    fprintf(fp, "%s\n", "ap_scan=1");
    fprintf(fp, "%s\n", "network={");
    fprintf(fp, "%s%s\n", "ssid=", ssid);
    fprintf(fp, "%s%s\n", "psk=", pwd);
    fprintf(fp, "%s\n", "key_mgmt=WPA-PSK");
    fprintf(fp, "%s\n", "}");

    fclose(fp);
    printf("write wifi cfg file finish!!!\n");
    if (-1 == system("killall wpa_supplicant;killall dhcpcd;"
                   "ifconfig wlan0 0.0.0.0")) {
        APP_ERROR0("killall wpa_supplicant dhcpcd failed");
        return FALSE;
    }

    if (-1 == system("wpa_supplicant -Dnl80211 -i wlan0 "
                   "-c /data/cfg/wpa_supplicant.conf &")) {
        APP_ERROR0("start wpa_supplicant failed");
        return FALSE;
    }

    if (-1 == system("sleep 1;dhcpcd wlan0 -t 0 &")) {
        APP_ERROR0("dhcpcd failed");
        return FALSE;
    }

    return TRUE;
}

/*******************************************************************************
**
** Function         app_ble_sys_manager_ccc_callback
**
** Description      APP BLE wifi introducer CCC callback.
**
** Returns          void
**
*******************************************************************************/
static void app_ble_sys_manager_ccc_callback(tBSA_BLE_EVT event,
                  tBSA_BLE_MSG *p_data)
{
    if (event == BSA_BLE_SE_WRITE_EVT)
    {
        APP_INFO0("app_ble_sys_manager_ccc_callback");
        APP_INFO1("sys_manager_characteristic_notify_characteristic_client_configuration = %d" ,
           sys_manager_characteristic_notify_characteristic_client_configuration);
    }
}

/*******************************************************************************
**
** Function         app_ble_sys_manager_send_notification_in_order
**
** Description      send notification split to 20 byte every package
**
** Returns          int
**
*******************************************************************************/
static int app_ble_sys_manager_send_notification_in_order(char * data,int length)
{
    tBSA_STATUS status;
    tBSA_BLE_SE_SENDIND ble_sendind_param;
    char cur_order = 0x00;
    char last_order = 0x00;
    int len_need_send = length;

    APP_INFO0("app_ble_sys_manager_send_notification");
    while(len_need_send > 0){
        status = BSA_BleSeSendIndInit(&ble_sendind_param);
        if (status != BSA_SUCCESS)
        {
            APP_ERROR1("BSA_BleSeSendIndInit failed status = %d", status);
            return -1;
        }

        ble_sendind_param.conn_id = app_ble_sys_manager_cb.conn_id;
        if (attr_index_notify >= APP_BLE_WIFI_INTRODUCER_ATTRIBUTE_MAX)
        {
            APP_ERROR0("Wrong attr_index_notify");
            return -1;
        }
        ble_sendind_param.attr_id = app_ble_sys_manager_cb.attr[attr_index_notify].attr_id;
        if (app_ble_sys_manager_cb.attr[attr_index_notify].val_len > BSA_BLE_SE_WRITE_MAX)
        {
            APP_ERROR1("Wrong Notification Value Length %d", app_ble_sys_manager_cb.attr[attr_index_notify].val_len);
            return -1;
        }
        if(len_need_send > 18){
            ble_sendind_param.data_len = 20;
            ble_sendind_param.value[0] = cur_order;
            ble_sendind_param.value[1] = last_order;
            memcpy(ble_sendind_param.value+2, (data+(length - len_need_send)), ble_sendind_param.data_len-2);
            len_need_send -= 18;
            last_order = cur_order;
            cur_order++;
            if(cur_order >= 0xff)
                cur_order = 0;
        }else{
            ble_sendind_param.data_len = len_need_send+2;
            ble_sendind_param.value[0] = 0xff;
            ble_sendind_param.value[1] = last_order;
            memcpy(ble_sendind_param.value+2, (data+(length - len_need_send)), ble_sendind_param.data_len-2);
            cur_order = 0;
            last_order = 0;
            len_need_send = 0;
        }
        ble_sendind_param.need_confirm = FALSE; // Notification
        APP_DUMP("send notification", (UINT8*)ble_sendind_param.value, ble_sendind_param.data_len);

        status = BSA_BleSeSendInd(&ble_sendind_param);
        if (status != BSA_SUCCESS)
        {
            APP_ERROR1("BSA_BleSeSendInd failed status = %d", status);
            return -1;
        }
    }
    return 0;
}

/*******************************************************************************
**
** Function         ping_check
**
** Description      check network states
**
** Returns          BOOLEAN
**
*******************************************************************************/
static BOOLEAN ping_check(void)
{
    int i = 0;
    while(i < 1)
    {
        pid_t pid;
        if ((pid = vfork()) < 0)
        {
            printf("vfork error");
            return FALSE;
        }
        else if (pid == 0)
        {
            if ( execlp("ping", "ping","-c","1","119.75.217.109", (char*)0) < 0)
            {
                printf("execlp error\n");
                return FALSE;
            }
        }
        int stat;
        waitpid(pid, &stat, 0);
        printf("state:%d\n",stat);
        if (stat == 0)
        {
            return TRUE;
        }
        sleep(1);
        i++;
    }
    return FALSE;
}

/*******************************************************************************
**
** Function         app_ble_sys_manager_wifiinfo_handler
**
** Description      handle wifi cmd and set wifi info to system
**
** Returns          void
**
*******************************************************************************/
static void app_ble_sys_manager_wifiinfo_handler(cJSON * usr)
{
    cJSON *wssid = cJSON_GetObjectItem(usr,"wpid");
    cJSON *wpsw = cJSON_GetObjectItem(usr,"wppwd");
    char *ssid = cJSON_Print(wssid);
    char *psw = cJSON_Print(wpsw);
    BOOLEAN is_net_connected = FALSE;
    int try = 13;
    if(start_wpa_supplicant(ssid,psw))
    {
		app_ble_sys_manager_send_wifistatus_notification("true",ssid,1,1);
/*        while(try > 0 && (is_net_connected == FALSE)){
           if(ping_check() == TRUE) 
               is_net_connected = TRUE;
           try--;
        }
        if(is_net_connected == TRUE)
            app_ble_sys_manager_send_wifistatus_notification("true",ssid,1,1);
        else
            app_ble_sys_manager_send_wifistatus_notification("true",ssid,1,0);
 */
    }else{
        app_ble_sys_manager_send_wifistatus_notification("fail",ssid,0,0);
    }
    printf("%s\n",ssid);
    printf("%s\n",psw);
}

char* substring(char* ch,int pos,int length)  
{  
    //定义字符指针 指向传递进来的ch地址
    char* pch=ch;  
    //通过calloc来分配一个length长度的字符数组，返回的是字符指针。
    char* subch=(char*)calloc(sizeof(char),length+1);  
    int i;  
 //只有在C99下for循环中才可以声明变量，这里写在外面，提高兼容性。  
    pch=pch+pos;  
//是pch指针指向pos位置。  
    for(i=0;i<length;i++)  
    {  
        subch[i]=*(pch++);  
//循环遍历赋值数组。  
    }  
    subch[length]='\0';//加上字符串结束符。  
    return subch;       //返回分配的字符数组地址。  
} 

static int app_ble_sys_manager_send_wifistatus_notification(char *cmd_state,char *wifi_wpid,int cnn_state,int net_state)
{
	char* newSsid={0};
	newSsid = substring(wifi_wpid,1,strlen(wifi_wpid)-3);
	
    cJSON * usr;
    usr=cJSON_CreateObject();   //创建根数据对象
    cJSON_AddStringToObject(usr,"cmdtype","wifistatus");
    cJSON_AddStringToObject(usr,"status",cmd_state);
    cJSON_AddStringToObject(usr,"msg","ack");
    cJSON_AddNumberToObject(usr,"wifi",cnn_state);  //加整数
    cJSON_AddStringToObject(usr,"wifi_wpid",newSsid);  //加整数
//    cJSON_AddNumberToObject(usr,"net",net_state);  //加整数
    char *out = cJSON_PrintUnformatted(usr);   //将json形式打印成正常字符串形式
    printf("%s\n",out);
    app_ble_sys_manager_send_notification_in_order(out,strlen(out));
    // 释放内存  
    cJSON_Delete(usr);  
    free(out);
    return 0;
}


static void app_ble_sys_manager_getsystemstatus_handler(cJSON * usr)
{
    tAPP_BLE_SYS_MANAGER_SYS_INFO info;
    char system_time[30] = {0};
    //char *mac_addr = sys_manager_mac_addr;
    char device_sn[50] = {0};
    char version[128] = "SystemVersion:";
    char wifi_wpid[128] = "ssid=";
    char ip_addr[IP_SIZE] = {0};
    double disk_free = 0;
    char upload_pid[10]={0};
    char rec_pid[10]={0};
    char cmd_buf[1024]={0};
    int rsoc = 0;
    int rsoc_h = 0;
    int rsoc_l = 0;
    cJSON  *pJsonArry;
    pJsonArry=cJSON_CreateArray();   /*创建数组*/
    get_file_name_by_path("/data",pJsonArry);
    char *out = cJSON_Print(pJsonArry);   //将json形式打印成正常字符串形式
    get_local_ip("wlan0",ip_addr);
    get_system_time(system_time);
    get_verdor_parameter(VENDOR_SN_ID,device_sn,20);
    info.uuid = device_sn;
    find_str_by_path(version,"/oem/version",":");
	
	char newVersion[128]={0};
	strncpy(newVersion, version, strlen(version)-1);
	printf("app_ble_sys_manager_getsystemstatus_handler() newVersion = %s\n",newVersion);

    info.version = newVersion;
    info.time = system_time;
    cmd_system(cmd_buf,"cat /sys/class/power_supply/battery/capacity");
    printf("rsoc:%x-%x-%x\n",cmd_buf[0],cmd_buf[1],cmd_buf[2]);
    if(cmd_buf[1]>0x30)
        rsoc_l = cmd_buf[1]-0x30;
    if(cmd_buf[0]>0x30)
        rsoc_h = cmd_buf[0]-0x30;
    rsoc = rsoc_h*10+rsoc_l;
    info.rsoc = rsoc;
    cmd_system(cmd_buf,"cat /sys/class/power_supply/battery/status");
    printf("charger status:%x-%x-%x\n",cmd_buf[0],cmd_buf[3],cmd_buf[7]);
    if((cmd_buf[0] == 0x43) && (cmd_buf[3] == 0x72) && (cmd_buf[7] == 0x67))
        info.is_charge = 1;
    else
        info.is_charge = 0;
    get_pid_by_name(rec_pid, "sh");
    if(strlen(rec_pid)>=3)
        info.rec = 2;
    else
        info.rec = 0;
    if(strlen(ip_addr)> 3){
        info.wifi_state = 1;
    }else{
        info.wifi_state = 0;
    }
    find_str_by_path(wifi_wpid,"/data/cfg/wpa_supplicant.conf","=");
	char* newSsid={0};
	newSsid = substring(wifi_wpid,1,strlen(wifi_wpid)-3);
    info.wifi_wpid = newSsid;
	
    if(ping_check() == TRUE){
        info.net = 1;
    }else{
        info.net = 0;
    }
    get_pid_by_name(upload_pid, "szdvr");
    printf("pid:%s\n",upload_pid);
    if(strlen(out)>4 && info.net == 1 && strlen(upload_pid)>=3)
        info.upload = 1;
    else
        info.upload = 0;
    cJSON_Delete(pJsonArry);  
    free(out);
    get_system_tf_free("/userdata",&disk_free);
    info.df = (int)(disk_free/1048576);
    app_ble_sys_manager_send_getsystemstatus_notification(info);
}
static int app_ble_sys_manager_send_getsystemstatus_notification(tAPP_BLE_SYS_MANAGER_SYS_INFO info)
{
    cJSON * usr;
    usr=cJSON_CreateObject();   //创建根数据对象
    cJSON_AddStringToObject(usr,"cmdtype","systemstatus");
    cJSON_AddStringToObject(usr,"uuid",info.uuid);
    cJSON_AddStringToObject(usr,"v",info.version);
    cJSON_AddStringToObject(usr,"time",info.time);
    cJSON_AddNumberToObject(usr,"rsoc",info.rsoc);  //加整数
    cJSON_AddNumberToObject(usr,"is_charge",info.is_charge);  //加整数
    cJSON_AddNumberToObject(usr,"rec",info.rec);  //加整数
    cJSON_AddNumberToObject(usr,"wifi",info.wifi_state);  //加整数
    cJSON_AddStringToObject(usr,"wifi_wpid",info.wifi_wpid);
    cJSON_AddNumberToObject(usr,"net",info.net);  //加整数
    cJSON_AddNumberToObject(usr,"upload",info.upload);  //加整数
    cJSON_AddNumberToObject(usr,"df",info.df);  //加整数
    char *out = cJSON_PrintUnformatted(usr);   //将json形式打印成正常字符串形式
    printf("%s\n",out);
    app_ble_sys_manager_send_notification_in_order(out,strlen(out));
    // 释放内存  
    cJSON_Delete(usr);  
    free(out);
    return 0;
}

static void app_ble_sys_manager_fileinfoget_handler(cJSON * usr)
{
    app_ble_sys_manager_send_fileinfoget_notification();
}
static int app_ble_sys_manager_send_fileinfoget_notification(void)
{
    cJSON * usr;
    cJSON  *pJsonArry;
    usr=cJSON_CreateObject();   //创建根数据对象
    pJsonArry=cJSON_CreateArray();   /*创建数组*/
    cJSON_AddStringToObject(usr,"cmdtype","filestatus");
    cJSON_AddStringToObject(usr,"msg","ack");
    get_file_name_by_path("/data/recorded",pJsonArry);
    cJSON_AddItemToObject(usr,"files",pJsonArry);
    char *out = cJSON_PrintUnformatted(usr);   //将json形式打印成正常字符串形式
    app_ble_sys_manager_send_notification_in_order(out,strlen(out));
    printf("%s\n",out);
    // 释放内存  
    //cJSON_Delete(files); 
    cJSON_Delete(usr);  
    free(out);
    return 0;
}

static void app_ble_sys_manager_getkey_handler(cJSON * usr)
{
    app_ble_sys_manager_send_getkey_notification();
}
static int app_ble_sys_manager_send_getkey_notification(void)
{
    cJSON * usr;
    usr=cJSON_CreateObject();   //创建根数据对象
    cJSON_AddStringToObject(usr,"cmdtype","keydata");
    uint8_t str_enc[]  = "dirfjiki889978yuefikjudefsligjciejkuifhjfdexfuhy78543d8906juefd";
    uint8_t send_str_enc[130] = {0};
    aes_encrypt_cbc(str_enc,sizeof(str_enc));
    send_str_enc[0]='"';
    int k = 0,j = 1;
    for(k=0;k<sizeof(str_enc);k++){
        send_str_enc[j] = hex_to_char((str_enc[k]&0xff) >> 4);
        j++;
        send_str_enc[j] = hex_to_char(str_enc[k]&0x0f);
        j++;
        printf("0x%x ",str_enc[k]);
        //printf("%c%c ",send_str_enc[j-2],send_str_enc[j-1]);
    }
    send_str_enc[129]='"';
    printf("\n");
    printf("enc string:%s\n",send_str_enc);
    cJSON_AddRawToObject(usr,"data",send_str_enc);
    char *out = cJSON_PrintUnformatted(usr);   //将json形式打印成正常字符串形式
    printf("%s\n",out);
    app_ble_sys_manager_send_notification_in_order(out,strlen(out));
    // 释放内存  
    cJSON_Delete(usr);  
    free(out);
    return 0;
}

static void app_ble_sys_manager_keydata_handler(cJSON * usr)
{
    uint8_t out[] = { 0x6b, 0xc1, 0xbe, 0xe2, 0x2e, 0x40, 0x9f, 0x96, 0xe9, 0x3d, 0x7e, 0x11, 0x73, 0x93, 0x17, 0x2a,
        0xae, 0x2d, 0x8a, 0x57, 0x1e, 0x03, 0xac, 0x9c, 0x9e, 0xb7, 0x6f, 0xac, 0x45, 0xaf, 0x8e, 0x51,
        0x30, 0xc8, 0x1c, 0x46, 0xa3, 0x5c, 0xe4, 0x11, 0xe5, 0xfb, 0xc1, 0x19, 0x1a, 0x0a, 0x52, 0xef,
        0xf6, 0x9f, 0x24, 0x45, 0xdf, 0x4f, 0x9b, 0x17, 0xad, 0x2b, 0x41, 0x7b, 0xe6, 0x6c, 0x37, 0x10};
    cJSON *type = cJSON_GetObjectItem(usr,"data");
    char *data = cJSON_Print(type);
    int k = 0;
    for(k=0;k<sizeof(out);k++)
        printf("%x ",data[k]);
    printf("\n");
    if (0 == memcmp((char*) out, (char*) data, sizeof(out))) {
        app_ble_sys_manager_send_keydata_notification("true");
    }else{
        app_ble_sys_manager_send_keydata_notification("false");
    }
    free(data);
}
static int app_ble_sys_manager_send_keydata_notification(char * result)
{
    cJSON * usr;
    usr=cJSON_CreateObject();   //创建根数据对象
    cJSON_AddStringToObject(usr,"cmdtype","authstatus");
    cJSON_AddStringToObject(usr,"status",result);
    char *out = cJSON_PrintUnformatted(usr);   //将json形式打印成正常字符串形式
    printf("%s\n",out);
    app_ble_sys_manager_send_notification_in_order(out,strlen(out));
    // 释放内存  
    cJSON_Delete(usr);  
    free(out);
    return 0;
}

static int get_spec_file_name(char *filename,char *postfix)
{
	int i=0,j=0;
	i = strlen(filename);
	j = strlen(postfix);
	if( i < j )
		return -1;
		
	for(;j>0;j--){
		if(*(filename+i-1) == *(postfix+j-1)){
			i--;
		}else{
			return -1;			
		}
	}
	return 0;
	
	
}

int file_size(char* filename)
{
    struct stat statbuf;
    stat(filename,&statbuf);
    int size=statbuf.st_size;
    return size;
}

static int get_file_name_by_path(char *basePath,cJSON *files)
{
     DIR *dir;
     struct dirent *ptr;
 
     if ((dir=opendir(basePath)) == NULL)
     {
         perror("Open dir error...");
         exit(1);
     }
     while ((ptr=readdir(dir)) != NULL)
     {
         if(strcmp(ptr->d_name,".")==0 || strcmp(ptr->d_name,"..")==0)    ///current dir OR parrent dir
             continue;
         else if(ptr->d_type == 8){    ///file
			 if(get_spec_file_name(ptr->d_name,".pcm") == 0){
                //printf("d_name:%s/%s\n",basePath,ptr->d_name);
                char local_filename[512] = {0};
                cJSON  *pJsonsub=cJSON_CreateObject();
                strcat(local_filename,basePath);
                strcat(local_filename,"/");
                strcat(local_filename,ptr->d_name);
                //get file size
                int filesize = file_size(local_filename);
                float m_size = ((float)filesize)/1048576;
                cJSON_AddStringToObject(pJsonsub,"name",ptr->d_name);
                cJSON_AddNumberToObject(pJsonsub,"size",m_size);
                cJSON_AddItemToArray(files,pJsonsub); /* 给创建的数组增加对对象*/
                //printf("file size:%d\n",m_size);
			  }
         }else if(ptr->d_type == 10)    ///link file
             printf("d_name:%s/%s\n",basePath,ptr->d_name);
         else if(ptr->d_type == 4)    ///dir
         {
            continue;
         }
     }
     closedir(dir);
     return 1;
}

static int get_name_from_mac(void)
{
    struct ifreq ifreq;
    int sock;

    if((sock=socket(AF_INET,SOCK_STREAM,0))<0)
    {
            perror("socket");
            return 2;
    }
    strcpy(ifreq.ifr_name,"wlan0");
    if(ioctl(sock,SIOCGIFHWADDR,&ifreq)<0)
    {
            perror("ioctl");
            return 3;
    }
    /*printf("%02x:%02x:%02x:%02x:%02x:%02x\n",
                    (unsigned char)ifreq.ifr_hwaddr.sa_data[0],
                    (unsigned char)ifreq.ifr_hwaddr.sa_data[1],
                    (unsigned char)ifreq.ifr_hwaddr.sa_data[2],
                    (unsigned char)ifreq.ifr_hwaddr.sa_data[3],
                    (unsigned char)ifreq.ifr_hwaddr.sa_data[4],
                    (unsigned char)ifreq.ifr_hwaddr.sa_data[5]);
                    */
    sprintf(sys_manager_mac_addr,"%x-%x-%x-%x-%x-%x",ifreq.ifr_hwaddr.sa_data[0],ifreq.ifr_hwaddr.sa_data[1],\
            ifreq.ifr_hwaddr.sa_data[2],ifreq.ifr_hwaddr.sa_data[3],ifreq.ifr_hwaddr.sa_data[4],ifreq.ifr_hwaddr.sa_data[5]);
    sprintf(sys_manager_device_name, "3308_manager_%x%x", ifreq.ifr_hwaddr.sa_data[4], ifreq.ifr_hwaddr.sa_data[5]);
    return 0;
}

static int get_verdor_parameter(int cmd, char *param_buf, int len)
{
    int sys_fd = -1;
    int ret = -1;
    RK_VERDOR_PROTOCOL_REQ req;

    printf("get_verdor_parameter: enter...\n");
    sys_fd = open(VERDOR_DEVICE, O_RDWR, 0);
    if (sys_fd < 0) {
        printf("get_verdor_parameter: error with sys_fd < 0\n");
        return ret;
    }
    memset(param_buf, 0, len);

    req.tag = VENDOR_REQ_TAG;
    req.id = (unsigned short)cmd;
    req.len = 512;
    if (ioctl(sys_fd, VENDOR_READ_IO, &req)) {
        printf("get_verdor_parameter: cmd = [%d] fail\n", cmd);
        printf("\t VENDOR_SN_ID        = 1\n");
        printf("\t VENDOR_WIFI_MAC_ID  = 2\n");
        printf("\t VENDOR_LAN_MAC_ID   = 3\n");
        printf("\t VENDOR_BLUETOOTH_ID = 4\n");
        close(sys_fd);
        return -1;
    }
    /* rknand_print_hex_data("vendor read:", (uint32_t*)&req, req.len + 8); **/
    if (req.len > len) {
        printf("get_verdor_parameter:%d force to %d\n", req.len, len);
        req.len = len;
        ret = len;
    } else {
        printf("get_verdor_parameter req.len:%d  len = %d\n", req.len, len);
    }

    switch (cmd) {
        case VENDOR_SN_ID:
            memcpy(param_buf, req.data, req.len);
            break;

        case VENDOR_WIFI_MAC_ID:
        case VENDOR_BLUETOOTH_ID:
            //Hex2Str(req.data, param_buf, req.len);
            break;

        default:
            break;
    }

    close(sys_fd);
    printf("get_verdor_parameter: success with cmd = [%d], paramter = [%s]\n",cmd, param_buf);

    return ret;
}

static int get_system_time(char *sys_time)
{
    time_t tmpcal_ptr;
    struct tm *tmp_ptr = NULL;
    time(&tmpcal_ptr);
    tmp_ptr = localtime(&tmpcal_ptr);
    printf ("after localtime, the time is:%d.%d.%d ", (1900+tmp_ptr->tm_year), (1+tmp_ptr->tm_mon), tmp_ptr->tm_mday);
    printf("%d:%d:%d\n", tmp_ptr->tm_hour, tmp_ptr->tm_min, tmp_ptr->tm_sec);
    sprintf(sys_time, "%d-%d-%d %d:%d:%d", (1900+tmp_ptr->tm_year), (1+tmp_ptr->tm_mon), tmp_ptr->tm_mday,tmp_ptr->tm_hour, \
            tmp_ptr->tm_min, tmp_ptr->tm_sec);
    return 0;	
}

static int spilt_string(char *string,const char *split)
{
    int i=0;
    char *p;
    p = strtok(string,split);
    while(p)
    {
        if(i == 1)
        {
            strcpy(string,p);
            printf(" is : %s \n",string);
            return 0;
        }
        i++;
        p=strtok(NULL,split);
    }
    return -1;
}
static int find_str_by_path(char *find,char *path,const char *split)
{
    FILE *fp = NULL;
    char *p, buffer[128]={0}; //初始化
    int ret;
    fp = fopen(path, "r");
    if(fp == NULL)
    {
        printf("open file failed.\n");
        return -1;
    }
    //memset(buffer, 0, sizeof(buffer));
    fseek(fp, 0, SEEK_SET);
    while(fgets(buffer, 128, fp) != NULL)
    {
        p = strstr(buffer, find);
        if(p)
        {
            printf("string is :%s \n",p);
            ret = spilt_string(p,split);
            if(ret == 0)
            {
                memset(find, 0, 128);
                printf("string len :%d \n",strlen(p));
                strncpy(find,p,strlen(p));
                return 0;
            }
        }
        memset(buffer, 0, sizeof(buffer));
    }
    fclose(fp);
    return -1;
}

static int get_local_ip(const char *eth_inf, char *ip)
{
	int sd;
	struct sockaddr_in sin;
	struct ifreq ifr;
 	sd = socket(AF_INET, SOCK_DGRAM, 0);
	if (-1 == sd)
	{
		printf("socket error: %s\n", strerror(errno));
		return -1;		
	}
 	strncpy(ifr.ifr_name, eth_inf, IFNAMSIZ);
	ifr.ifr_name[IFNAMSIZ - 1] = 0;
		// if error: No such device
	if (ioctl(sd, SIOCGIFADDR, &ifr) < 0)
	{
		printf("ioctl error: %s\n", strerror(errno));
		close(sd);
		return -1;
	}
 	memcpy(&sin, &ifr.ifr_addr, sizeof(sin));
	snprintf(ip, IP_SIZE, "%s", inet_ntoa(sin.sin_addr));
    printf("ip:%s",ip);
 	close(sd);
	return 0;
}

static int get_system_tf_free(char *disk,double *free)
{
    if(free == NULL)
            return -1;
    struct statfs diskInfo;
    statfs(disk,&diskInfo);
    unsigned long long totalBlocks = diskInfo.f_bsize;
    unsigned long long freeDisk = diskInfo.f_bfree*totalBlocks;

    *free = freeDisk;
    return 0;
}


static void get_pid_by_name(char *pid, char *task_name)
 {
    DIR *dir;
    struct dirent *ptr;
    FILE *fp;
    char filepath[50];//大小随意，能装下cmdline文件的路径即可
    char filetext[50];//大小随意，能装下要识别的命令行文本即可

    dir = opendir("/proc"); //打开路径
    if (NULL != dir)
    {
        while ((ptr = readdir(dir)) != NULL) //循环读取路径下的每一个文件/文件夹
        {

            //如果读取到的是"."或者".."则跳过，读取到的不是文件夹名字也跳过
            if ((strcmp(ptr->d_name, ".") == 0) || (strcmp(ptr->d_name, "..") == 0)) continue;
            if (DT_DIR != ptr->d_type) continue;
           
            sprintf(filepath, "/proc/%s/cmdline", ptr->d_name);//生成要读取的文件的路径
            fp = fopen(filepath, "r");//打开文件

            if (NULL != fp)
            {
                fread(filetext, 1, 50, fp);//读取文件
                filetext[49] = '\0';//给读出的内容加上字符串结束符

                //如果文件内容满足要求则打印路径的名字（即进程的PID）
                //printf("file txt:%s\n",filetext);
                if (filetext == strstr(filetext, task_name)){ 
                    printf("PID:  %s\n", ptr->d_name);
                    memcpy(pid,ptr->d_name,strlen(ptr->d_name));
                    fclose(fp);
                    closedir(dir);//关闭路径
                    return;
                }
                fclose(fp);
            }
        }

        closedir(dir);//关闭路径
    }
 }

static int led_pwm_set(char *channel,int period,float rate)
{
    int PwmChannel;
    int MyPeriod = period;
    int MyDuty = MyPeriod * rate;
	char *array[3] = {"G", "R", "B"};

    
	if(!strcmp(channel,array[0])){
		PwmChannel = 0;
        if(pwm_led_set(1,0) < 0){
            printf("PWM led set error!\n");
            return(-1);
        }
        if(pwm_led_set(2,0) < 0){
            printf("PWM led set error!\n");
            return(-1);
        }
        printf("PWM_G successfully enabled with period - %dms, duty cycle - %2.1f%%\n", MyPeriod/1000000, rate*100);
    }
	else if(!strcmp(channel,array[1])){
		PwmChannel = 1;
        if(pwm_led_set(0,0) < 0){
            printf("PWM led set error!\n");
            return(-1);
        }
        if(pwm_led_set(2,0) < 0){
            printf("PWM led set error!\n");
            return(-1);
        }
		printf("PWM_R successfully enabled with period - %dms, duty cycle - %2.1f%%\n", MyPeriod/1000000, rate*100);
	}
	else if(!strcmp(channel,array[2])){
		PwmChannel = 2;
        if(pwm_led_set(1,0) < 0){
            printf("PWM led set error!\n");
            return(-1);
        }
        if(pwm_led_set(0,0) < 0){
            printf("PWM led set error!\n");
            return(-1);
        }
		printf("PWM_B successfully enabled with period - %dms, duty cycle - %2.1f%%\n", MyPeriod/1000000, rate*100);
	}
	else{
		printf("wrong PWM Channel input\n");
		return(-1);
    }
    if(pwm_led_set(PwmChannel,50) < 0){
        printf("PWM led set error!\n");
        return(-1);
    }
	return 0;
} 

static int cmd_system(char *result,const char* command)
{
    FILE *fpRead;
    fpRead = popen(command, "r");
    char buf[1024];
    memset(buf,'\0',sizeof(buf));
    while(fgets(buf,1024-1,fpRead)!=NULL)
    { 
        memcpy(result,buf,1024);
    }
    if(fpRead!=NULL)
        pclose(fpRead);
    printf("result:%s\n",result);
    return 0;
} 

int led_set_color(char *channel,int period,int rate)
{
	char *array[3] = {"G", "R", "B"};
	if(!strcmp(channel,array[0])){
        write_led_info("G",period,rate);
    }
	else if(!strcmp(channel,array[1])){
        write_led_info("R",period,rate);
	}
	else if(!strcmp(channel,array[2])){
        write_led_info("B",period,rate);
	}
	else{
		printf("wrong color input\n");
		return(-1);
    }
	return 0;
}

 int read_file(char *path,char *buf,int len){
    int fd = open(path,O_RDONLY);
    if(fd==-1)
    {
        printf("error is %s\n",strerror(errno));
        return -1;
    }
    else
    {
        printf("read success fd = %d\n",fd);
        memset(buf,0,len);
        while(read(fd,buf,len)>0)
        {
            printf("%s\n", buf);
        }
        close(fd);
    }
    return 0;
 }
 
 int write_file(char *path,char *buf){
	int fd=open(path,O_RDWR|O_CREAT,0777);
	if(fd==-1)
	{
		printf("error is %s\n",strerror(errno));
        return -1;
	}
	else
	{
		//打印文件描述符号
		printf("success fd = %d\n",fd);
		write(fd,buf,strlen(buf));
		close(fd);
	}
	return 0;
 }
 
static int write_led_info(char * color,int period,int rate)
{
    cJSON * usr;
    usr=cJSON_CreateObject();   //创建根数据对象
    cJSON_AddStringToObject(usr,"color",color);
    cJSON_AddNumberToObject(usr,"period",period);
    cJSON_AddNumberToObject(usr,"rate",rate);
    char *out = cJSON_PrintUnformatted(usr);   //将json形式打印成正常字符串形式
    printf("write file: %s\n",out);
    write_file("/oem/led.json",out);
    // 释放内存  
    //cJSON_Delete(files); 
    cJSON_Delete(usr);  
    free(out);
    return 0;
}

static int aes_encrypt_cbc(uint8_t *in,int len)
{
    uint8_t key[] = "jbs001forsz01956";//jbs001forsz01956
    uint8_t iv[]  = "0123456789abcdef";//0123456789abcdef
    struct AES_ctx ctx;

    AES_init_ctx_iv(&ctx, key, iv);
    AES_CBC_encrypt_buffer(&ctx, in, len);

    printf("CBC encrypt: ");
    printf("SUCCESS!\n");
	return(0);
}

static int aes_decrypt_cbc(uint8_t *in,int len)
{
    uint8_t key[] = "jbs001forsz01956";//jbs001forsz01956
    uint8_t iv[]  = "0123456789abcdef";//0123456789abcdef
//  uint8_t buffer[64];
    struct AES_ctx ctx;

    AES_init_ctx_iv(&ctx, key, iv);
    AES_CBC_decrypt_buffer(&ctx, in, len);

    printf("CBC decrypt: ");
    printf("SUCCESS!\n");
	return(0);
} 

static uint8_t hex_to_char(uint8_t temp)
{
    uint8_t dst;
    //printf("hex:%x ",temp);
    if (temp < 10){
        dst = temp + '0';
    }else{
        dst = temp -10 +'A';
    }
    return dst;
}
