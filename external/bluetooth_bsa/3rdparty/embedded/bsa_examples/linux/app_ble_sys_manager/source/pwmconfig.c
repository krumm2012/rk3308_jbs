#include "pwmconfig.h"

/* PWM export */
int pwm_export(unsigned int pwm)
{
	int fd;

	switch(pwm){
	case 0:{
		fd = open(SYSFS_PWM_DIR "/pwmchip0/export", O_WRONLY);
		if (fd < 0) {
			printf ("\nFailed export PWM_0\n");
			return -1;
		}

		write(fd, "0", 2);
		close(fd);
		return 0;
		break;

	}
	case 1:{
		fd = open(SYSFS_PWM_DIR "/pwmchip1/export", O_WRONLY);
		if (fd < 0) {
			printf ("\nFailed export PWM_1\n");
			return -1;
		}

		write(fd, "0", 2);
		close(fd);
		return 0;
		break;
	}
	case 2:{
		fd = open(SYSFS_PWM_DIR "/pwmchip2/export", O_WRONLY);
		if (fd < 0) {
			printf ("\nFailed export PWM_2\n");
			return -1;
		}

		write(fd, "0", 2);
		close(fd);
		return 0;
		break;
	}
	case 3:{
		fd = open(SYSFS_PWM_DIR "/pwmchip3/export", O_WRONLY);
		if (fd < 0) {
			printf ("\nFailed export PWM_3\n");
			return -1;
		}

		write(fd, "0", 2);
		close(fd);
		return 0;
		break;
	}
	default:{
		printf("wrong PWM channel input\n");
		return -1;
	}
	}
}

/* PWM unexport */
int pwm_unexport(unsigned int pwm)
{
	int fd;

	switch(pwm){
	case 0:{
		fd = open(SYSFS_PWM_DIR "/pwmchip0/export", O_WRONLY);
		if (fd < 0) {
			printf ("\nFailed unexport PWM_0\n");
			return -1;
		}

		write(fd, "1", 2);
		close(fd);
		return 0;
        break;
	}
	case 1:{
		fd = open(SYSFS_PWM_DIR "/pwmchip1/export", O_WRONLY);
		if (fd < 0) {
			printf ("\nFailed unexport PWM_1\n");
			return -1;
		}

		write(fd, "1", 2);
		close(fd);
		return 0;
		break;
	}
	case 2:{
		fd = open(SYSFS_PWM_DIR "/pwmchip2/export", O_WRONLY);
		if (fd < 0) {
			printf ("\nFailed unexport PWM_2\n");
			return -1;
		}

		write(fd, "1", 2);
		close(fd);
		return 0;
		break;
	}
	case 3:{
		fd = open(SYSFS_PWM_DIR "/pwmchip3/export", O_WRONLY);
		if (fd < 0) {
			printf ("\nFailed unexport PWM_3\n");
			return -1;
		}

		write(fd, "1", 2);
		close(fd);
		return 0;
		break;
	}
	default:{
		printf("wrong PWM channel input\n");
		return -1;
	}
	}
}

/* PWM configuration */
int pwm_config(unsigned int pwm, unsigned int period, unsigned int duty_cycle)
{
	int fd,len_p,len_d;
	char buf_p[MAX_BUF];
	char buf_d[MAX_BUF];

	len_p = snprintf(buf_p, sizeof(buf_p), "%d", period);
	len_d = snprintf(buf_d, sizeof(buf_d), "%d", duty_cycle);

	switch(pwm){
	case 0:{
			/* set pwm period */
		    fd = open(SYSFS_PWM_DIR "/pwmchip0/pwm0/period", O_WRONLY);
			if (fd < 0) {
				printf ("\nFailed set PWM_0 period\n");
				return -1;
			}

			write(fd, buf_p, len_p);
			/* set pwm duty cycle */
			fd = open(SYSFS_PWM_DIR "/pwmchip0/pwm0/duty_cycle", O_WRONLY);
			if (fd < 0) {
				printf ("\nFailed set PWM_0 duty cycle\n");
				return -1;
			}

			write(fd, buf_d, len_d);

			close(fd);
			return 0;
			break;
		}
		case 1:{
			/* set pwm period */
			fd = open(SYSFS_PWM_DIR "/pwmchip1/pwm0/period", O_WRONLY);
			if (fd < 0) {
				printf ("\nFailed set PWM_1 period\n");
				return -1;
			}

			write(fd, buf_p, len_p);
			/* set pwm duty cycle */
			fd = open(SYSFS_PWM_DIR "/pwmchip1/pwm0/duty_cycle", O_WRONLY);
			if (fd < 0) {
				printf ("\nFailed set PWM_1 duty cycle\n");
				return -1;
			}

			write(fd, buf_d, len_d);

			close(fd);
			return 0;
			break;
		}
		case 2:{
			/* set pwm period */
			fd = open(SYSFS_PWM_DIR "/pwmchip2/pwm0/period", O_WRONLY);
			if (fd < 0) {
				printf ("\nFailed set PWM_2 period\n");
				return -1;
			}

			write(fd, buf_p, len_p);
			/* set pwm duty cycle */
			fd = open(SYSFS_PWM_DIR "/pwmchip2/pwm0/duty_cycle", O_WRONLY);
			if (fd < 0) {
				printf ("\nFailed set PWM_2 duty cycle\n");
				return -1;
			}

			write(fd, buf_d, len_d);

			close(fd);
			return 0;
			break;
		}
		case 3:{
			/* set pwm period */
			fd = open(SYSFS_PWM_DIR "/pwmchip3/pwm0/period", O_WRONLY);
			if (fd < 0) {
				printf ("\nFailed set PWM_3 period\n");
				return -1;
			}

			write(fd, buf_p, len_p);
			/* set pwm duty cycle */
			fd = open(SYSFS_PWM_DIR "/pwmchip3/pwm0/duty_cycle", O_WRONLY);
			if (fd < 0) {
				printf ("\nFailed set PWM_3 duty cycle\n");
				return -1;
			}

			write(fd, buf_d, len_d);

			close(fd);
			return 0;
			break;
		}
		default:{
			printf("wrong PWM channel input\n");
			return -1;
		}
		}
}

/* PWM enable */
int pwm_enable(unsigned int pwm)
{
	int fd;

	switch(pwm){
	case 0:{
		    fd = open(SYSFS_PWM_DIR "/pwmchip0/pwm0/enable", O_WRONLY);
			if (fd < 0) {
				printf ("\nFailed enable PWM_0\n");
				return -1;
			}
			write(fd, "1", 2);

			close(fd);
			return 0;
			break;
		}
		case 1:{
			fd = open(SYSFS_PWM_DIR "/pwmchip1/pwm0/enable", O_WRONLY);
			if (fd < 0) {
				printf ("\nFailed enable PWM_1\n");
				return -1;
			}
			write(fd, "1", 2);

			close(fd);
			return 0;
			break;
			}
		case 2:{
			fd = open(SYSFS_PWM_DIR "/pwmchip2/pwm0/enable", O_WRONLY);
			if (fd < 0) {
				printf ("\nFailed enable PWM_2\n");
				return -1;
			}
			write(fd, "1", 2);

			close(fd);
			return 0;
			break;
			}
		case 3:{
			fd = open(SYSFS_PWM_DIR "/pwmchip3/pwm0/enable", O_WRONLY);
			if (fd < 0) {
				printf ("\nFailed enable PWM_3\n");
				return -1;
			}
			write(fd, "1", 2);

			close(fd);
			return 0;
			break;
		}
		default:{
			printf("wrong PWM channel input\n");
			return -1;
		}
		}
}

int pwm_led_set(unsigned int pwm,unsigned int brightness)
{
	int fd,len_p;
	char buf_p[MAX_BUF];

	len_p = snprintf(buf_p, sizeof(buf_p), "%d", brightness);

	switch(pwm){
            case 0:{
                    /* set pwm brightness */
                    fd = open(SYSFS_PWM_LED_DIR "/PWM-G/brightness", O_WRONLY);
                    if (fd < 0) {
                        printf ("\nFailed set PWM-G brightness\n");
                        return -1;
                    }
                    write(fd, buf_p, len_p);
                    close(fd);
                    return 0;
                    break;
            }
            case 1:{
                /* set pwm brightness */
                fd = open(SYSFS_PWM_LED_DIR "/PWM-R/brightness", O_WRONLY);
                if (fd < 0) {
                    printf ("\nFailed set PWM-R brightness\n");
                    return -1;
                }
                write(fd, buf_p, len_p);
                close(fd);
                return 0;
                break;
            }
            case 2:{
                /* set pwm brightness */
                fd = open(SYSFS_PWM_LED_DIR "/PWM-B/brightness", O_WRONLY);
                if (fd < 0) {
                    printf ("\nFailed set PWM-B brightness\n");
                    return -1;
                }
                write(fd, buf_p, len_p);
                close(fd);
                return 0;
                break;
            }
            default:{
                printf("wrong PWM channel input\n");
                return -1;
            }
		}
}

/* PWM disable */
int pwm_disable(unsigned int pwm)
{
	int fd;

		switch(pwm){
		case 0:{
			    fd = open(SYSFS_PWM_DIR "/pwmchip0/pwm0/enable", O_WRONLY);
				if (fd < 0) {
					printf ("\nFailed disable PWM_0\n");
					return -1;
				}
				write(fd, "0", 2);

				close(fd);
				return 0;
				break;
			}
			case 1:{
				fd = open(SYSFS_PWM_DIR "/pwmchip1/pwm0/enable", O_WRONLY);
				if (fd < 0) {
					printf ("\nFailed disable PWM_1\n");
					return -1;
				}
				write(fd, "0", 2);

				close(fd);
				return 0;
				break;
				}
			case 2:{
				fd = open(SYSFS_PWM_DIR "/pwmchip2/pwm0/enable", O_WRONLY);
				if (fd < 0) {
					printf ("\nFailed disable PWM_2\n");
					return -1;
				}
				write(fd, "0", 2);

				close(fd);
				return 0;
				break;
				}
			case 3:{
				fd = open(SYSFS_PWM_DIR "/pwmchip3/pwm0/enable", O_WRONLY);
				if (fd < 0) {
					printf ("\nFailed disable PWM_3\n");
					return -1;
				}
				write(fd, "0", 2);

				close(fd);
				return 0;
				break;
			}
			default:{
				printf("wrong PWM channel input\n");
				return -1;
			}
			}
}
