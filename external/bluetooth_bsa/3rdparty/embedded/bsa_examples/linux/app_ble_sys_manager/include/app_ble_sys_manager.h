/*****************************************************************************
**
**  Name:           app_ble_sys_manager.h
**
**  Description:    Bluetooth BLE WiFi introducer include file
**
**  Copyright (c) 2018, Cypress Corp., All Rights Reserved.
**  Cypress Bluetooth Core. Proprietary and confidential.
**
*****************************************************************************/
#ifndef APP_BLE_SYS_MANAGER_H
#define APP_BLE_SYS_MANAGER_H

#include "bsa_api.h"
#include "app_ble.h"

#define APP_BLE_SYS_MANAGER_TIMEOUT 60 /* 6 seconds */
#define APP_BLE_SYS_MANAGER_GATT_ATTRIBUTE_SIZE (22)
typedef void (tAPP_BLE_WIFI_CBACK)(tBSA_BLE_EVT event, tBSA_BLE_MSG *p_data);
/* This is the default AP the device will connect to (as a client)*/
#define CLIENT_AP_SSID       "YOUR_AP_SSID"
#define CLIENT_AP_PASSPHRASE "YOUR_AP_PASSPHRASE"

#ifndef APP_BLE_WIFI_INTRODUCER_ATTRIBUTE_MAX
#define APP_BLE_WIFI_INTRODUCER_ATTRIBUTE_MAX BSA_BLE_ATTRIBUTE_MAX
#endif

#define VENDOR_REQ_TAG 0x56524551
#define VENDOR_READ_IO _IOW('v', 0x01, unsigned int)
#define VENDOR_WRITE_IO _IOW('v', 0x02, unsigned int)
/* Storage parameter */
#define VENDOR_PARAMETER_ID 5
/* Change the id when flash firmware */
#define VENDOR_FW_UPDATE_ID 6
#define VENDOR_DATA_SIZE (3 * 1024)  // 3k
#define VENDOR_DATA_PROTOCOL_SIZE (384)  /* 64*6=384 byte */
#define VENDOR_SN_ID 1
#define VENDOR_WIFI_MAC_ID 2
#define VENDOR_LAN_MAC_ID 3
#define VENDOR_BLUETOOTH_ID 4
#define VENDOR_STREAM_ID 14
#define VENDOR_PROTOCOL_ID  15
#define VERDOR_DEVICE "/dev/vendor_storage"

typedef struct
{
    tBT_UUID       attr_UUID;
    UINT16         service_id;
    UINT16         attr_id;
    UINT8          attr_type;
    tBSA_BLE_CHAR_PROP prop;
    tBSA_BLE_PERM       perm;
    BOOLEAN        is_pri;
    BOOLEAN        wait_flag;
    UINT16        val_len;
    UINT16        max_val_len;
    void            *p_val;
    tBSA_BLE_CBACK      *p_cback;
} tAPP_BLE_WIFI_INTRODUCER_ATTRIBUTE;

typedef struct 
{
    tBSA_BLE_IF         server_if;
    UINT16                conn_id;
    tAPP_BLE_WIFI_INTRODUCER_ATTRIBUTE  attr[APP_BLE_WIFI_INTRODUCER_ATTRIBUTE_MAX];
} tAPP_BLE_SYS_MANAGER_CB;

typedef struct
{
    char * uuid;
    char * version;
    char * time;
    int rsoc;
    int is_charge;
    int rec;
    int wifi_state;
    char * wifi_wpid;
    int net;
    int upload;
    int df;
} tAPP_BLE_SYS_MANAGER_SYS_INFO;

typedef struct _RK_VERDOR_PROTOCOL_REQ {
  uint32_t tag;
  uint16_t id;
  uint16_t len;
  uint8_t data[VENDOR_DATA_PROTOCOL_SIZE];
} RK_VERDOR_PROTOCOL_REQ;

/*******************************************************************************
 **
 ** Function         app_ble_sys_manager_init
 **
 ** Description     APP BLE wifi introducer control block init
 **
 ** Parameters     None
 **
 ** Returns          None
 **
 *******************************************************************************/
void app_ble_sys_manager_init(void);

/*******************************************************************************
 **
 ** Function         app_ble_sys_manager_gatt_server_init
 **
 ** Description     APP BLE wifi introducer GATT Server init
 **
 ** Parameters     None
 **
 ** Returns          None
 **
 *******************************************************************************/
void app_ble_sys_manager_gatt_server_init(void);

int led_set_color(char *channel,int period,int rate);
#endif
