/*
 * Simple app. to do memory accesses via /dev/mem.
 *
 * $Id: io.c,v 1.5 2000/08/21 09:01:57 richard Exp $
 *
 * Copyright (c) Richard Hirst <rhirst@linuxcare.com>
 */

#include <assert.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/reboot.h>
#include <sys/syscall.h>
#include <sys/ioctl.h>
#include <linux/watchdog.h>
#include <linux/reboot.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>


//add read uid
#define FWK_MSG_UID_LEN 20
#define VENDOR_REQ_TAG 0x56524551
#define VENDOR_READ_IO _IOW('v', 0x01, unsigned int)
#define VENDOR_WRITE_IO _IOW('v', 0x02, unsigned int)
#define VENDOR_SN_ID 1
#define VENDOR_DATA_PROTOCOL_SIZE (384)  /* 64*6=384 byte */
#define VENDOR_DATA_SIZE (3 * 1024)  // 3k
#define VERDOR_DEVICE "/dev/vendor_storage"
typedef struct _RK_VERDOR_REQ {
  uint32_t tag;
  uint16_t id;
  uint16_t len;
  uint8_t data[VENDOR_DATA_SIZE];
} RK_VERDOR_REQ;

typedef struct _RK_VERDOR_PROTOCOL_REQ {
  uint32_t tag;
  uint16_t id;
  uint16_t len;
  uint8_t data[VENDOR_DATA_PROTOCOL_SIZE];
} RK_VERDOR_PROTOCOL_REQ;
//

static int spilt_string(char *string,const char *split)
{
    int i=0;
    char *p;
    p = strtok(string,split);
    while(p)
    {
        if(i == 1)
        {
            strcpy(string,p);
            //printf(" is : %s \n",string);
            return 0;
        }
        i++;
        p=strtok(NULL,split);
    }
    return -1;
}


static int rk_parameter_read_uid(int sys_fd, char *iotc_uid, int len)
{
    RK_VERDOR_PROTOCOL_REQ req;

    if (sys_fd < 0) {
        printf("_read_uid: error with sys_fd < 0\n");
        return -1;
    }
    req.tag = VENDOR_REQ_TAG;
    req.id = VENDOR_SN_ID;
    req.len = 512;
    if (ioctl(sys_fd, VENDOR_READ_IO, &req)) {
        printf("_read_uid:VENDOR_SN_ID fail\n");
        return -1;
    }
    /* rknand_print_hex_data("vendor read:", (uint32_t*)&req, req.len + 8); **/
    if (req.len > len) {
        //printf("_read_uid:%d force to %d\n", req.len, len);
        req.len = len;
    }
    memcpy(iotc_uid, req.data, req.len);

    return req.len;
}

static int parameter_read_protocol_uid(char iotc_uid[FWK_MSG_UID_LEN+1])
{
    int sys_fd = -1;
    int ret = -1;
    sys_fd = open(VERDOR_DEVICE, O_RDWR, 0);
    if (sys_fd < 0) {
        printf("read_uid: error with sys_fd < 0\n");
        return ret;
    }
    memset(iotc_uid, 0, FWK_MSG_UID_LEN + 1);
    ret = rk_parameter_read_uid(sys_fd, &iotc_uid[0], FWK_MSG_UID_LEN);
    close(sys_fd);
    return ret;
}

static int find_str_by_path(char *find,char *path,const char *split)
{
    FILE *fp = NULL;
    char *p, buffer[128]={0}; //初始化
    int ret;
    fp = fopen(path, "r");
    if(fp == NULL)
    {
        printf("open file failed.\n");
        return -1;
    }
    //memset(buffer, 0, sizeof(buffer));
    fseek(fp, 0, SEEK_SET);
    while(fgets(buffer, 128, fp) != NULL)
    {
        p = strstr(buffer, find);
        if(p)
        {
            //printf("string is :%s \n",p);
            ret = spilt_string(p,split);
            if(ret == 0)
            {
                memset(find, 0, 128);
                //printf("string len :%d \n",strlen(p));
                strncpy(find,p,strlen(p));
                return 0;
            }
        }
        memset(buffer, 0, sizeof(buffer));
    }
    fclose(fp);
    return -1;
}

void getsyssn(void){
	char uid[FWK_MSG_UID_LEN + 1];
	char version[128] = "SystemVersion:";
	memset(uid, 0, sizeof(uid));
	if(parameter_read_protocol_uid(uid)>0)
	{
		uid[FWK_MSG_UID_LEN] = '\0';
		printf("Sn:%s\n",uid);
	}else{
		printf("not burn sn",uid);
	}
	find_str_by_path(version,"/oem/version",":");
	printf("Version:%s\n",version);
	
}




int main (int argc, char **argv)
{
	getsyssn();
	return 0;
}

