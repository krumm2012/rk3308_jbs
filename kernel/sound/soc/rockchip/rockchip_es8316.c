/*
 * Rockchip machine ASoC driver for boards using a es8316 CODEC.
 *
 * Copyright (c) 2015, ROCKCHIP CORPORATION.  All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/gpio.h>
#include <linux/of_gpio.h>
#include <linux/delay.h>
#include <sound/core.h>
#include <sound/jack.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>
#include "rockchip_i2s_tdm.h"

#define DRV_NAME "rockchip-snd-es8316"

//static struct snd_soc_jack headset_jack;

/* Jack detect via es8316 driver. */
extern int es8316_set_jack_detect(struct snd_soc_codec *codec,
	struct snd_soc_jack *hp_jack, struct snd_soc_jack *mic_jack,
	struct snd_soc_jack *btn_jack);

static const struct snd_soc_dapm_widget rk_dapm_widgets[] = {
	SND_SOC_DAPM_HP("Headphones", NULL),
	SND_SOC_DAPM_SPK("Speakers", NULL),
	SND_SOC_DAPM_MIC("Headset Mic", NULL),
	SND_SOC_DAPM_MIC("Int Mic", NULL),
};

static const struct snd_soc_dapm_route rk_audio_map[] = {
	/* Input Lines */
	{"DMIC L2", NULL, "Int Mic"},
	{"DMIC R2", NULL, "Int Mic"},
	{"RECMIXL", NULL, "Headset Mic"},
	{"RECMIXR", NULL, "Headset Mic"},

	/* Output Lines */
	{"Headphones", NULL, "HPOR"},
	{"Headphones", NULL, "HPOL"},
	{"Speakers", NULL, "SPOL"},
	{"Speakers", NULL, "SPOR"},
};

static const struct snd_kcontrol_new rk_mc_controls[] = {
	SOC_DAPM_PIN_SWITCH("Headphones"),
	SOC_DAPM_PIN_SWITCH("Speakers"),
	SOC_DAPM_PIN_SWITCH("Headset Mic"),
	SOC_DAPM_PIN_SWITCH("Int Mic"),
};

static int rk_es8316_hw_params(struct snd_pcm_substream *substream,
			     struct snd_pcm_hw_params *params)
{
    struct snd_soc_pcm_runtime *rtd = substream->private_data;
    struct snd_soc_dai *codec_dai = rtd->codec_dai;
    struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
    unsigned int pll_out = 0, dai_fmt = rtd->card->dai_link->dai_fmt;
    int ret;

    /* set codec DAI configuration */
    ret = snd_soc_dai_set_fmt(codec_dai, dai_fmt);
    if (ret < 0) {
        printk("%s():failed to set the format for codec side\n", __FUNCTION__);
        return ret;
    }

    /* set cpu DAI configuration */
    ret = snd_soc_dai_set_fmt(cpu_dai, dai_fmt);
    if (ret < 0) {
        printk("%s():failed to set the format for cpu side\n", __FUNCTION__);
        return ret;
    }

    switch(params_rate(params)) {
    case 8000:
    case 16000:
    case 24000:
    case 32000:
    case 48000:
        pll_out = 12288000;
        break;
    case 11025:
    case 22050:
    case 44100:
        pll_out = 11289600;
        break;
    default:
        dev_err(codec_dai->dev, "Enter:%s, %d, Error rate=%d\n",__FUNCTION__,__LINE__,params_rate(params));
        return -EINVAL;
        break;
    }
    dev_err(codec_dai->dev, "Enter:%s, %d, rate=%d\n",__FUNCTION__,__LINE__,params_rate(params));

    if ((dai_fmt & SND_SOC_DAIFMT_MASTER_MASK) == SND_SOC_DAIFMT_CBS_CFS) {
        snd_soc_dai_set_sysclk(cpu_dai, 0, pll_out, 0);
        snd_soc_dai_set_clkdiv(cpu_dai, ROCKCHIP_DIV_BCLK, (pll_out/4)/params_rate(params)-1);
        snd_soc_dai_set_clkdiv(cpu_dai, ROCKCHIP_DIV_MCLK, 3);
    }

    dev_err(codec_dai->dev, "Enter:%s, %d, LRCK=%d\n",__FUNCTION__,__LINE__,(pll_out/4)/params_rate(params));
    return 0;
}

static int rk_init(struct snd_soc_pcm_runtime *runtime)
{
    struct snd_soc_dai *codec_dai = runtime->codec_dai;
    int ret;

    dev_err(codec_dai->dev, "Enter::%s----%d\n",__FUNCTION__,__LINE__);
    ret = snd_soc_dai_set_sysclk(codec_dai, 0,
        12288000 /*11289600*/, SND_SOC_CLOCK_IN);
    if (ret < 0) {
        printk(KERN_ERR "Failed to set dac3100 SYSCLK: %d\n", ret);
        return ret;
    }

    return 0;
}

static struct snd_soc_ops rk_aif1_ops = {
	.hw_params = rk_es8316_hw_params,
};

static struct snd_soc_dai_link rk_dailink = {
	.name = "es8316",
	.stream_name = "es8316 PCM",
	.codec_dai_name = "ES8316 HiFi",
	.init = rk_init,
	.ops = &rk_aif1_ops,
	/* set es8316 as slave */
	.dai_fmt = SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF |
		SND_SOC_DAIFMT_CBS_CFS,
};

static struct snd_soc_card snd_soc_card_rk = {
	.name = "rockchip,es8316-audio-codec",
	.owner = THIS_MODULE,
	.dai_link = &rk_dailink,
	.num_links = 1,
	.dapm_widgets = rk_dapm_widgets,
	.num_dapm_widgets = ARRAY_SIZE(rk_dapm_widgets),
	.dapm_routes = rk_audio_map,
	.num_dapm_routes = ARRAY_SIZE(rk_audio_map),
	.controls = rk_mc_controls,
	.num_controls = ARRAY_SIZE(rk_mc_controls),
};

static int snd_rk_es8316_probe(struct platform_device *pdev)
{
	int ret = 0;
	struct snd_soc_card *card = &snd_soc_card_rk;
	struct device_node *np = pdev->dev.of_node;

	/* register the soc card */
	card->dev = &pdev->dev;
	printk("++++++++++++++++++++snd_rk_es8316_probe in\n");
	rk_dailink.codec_of_node = of_parse_phandle(np,
			"rockchip,audio-codec", 0);
	if (!rk_dailink.codec_of_node) {
		dev_err(&pdev->dev,
			"Property 'rockchip,audio-codec' missing or invalid\n");
		return -EINVAL;
	}

	rk_dailink.cpu_of_node = of_parse_phandle(np,
			"rockchip,i2s-controller", 0);
	if (!rk_dailink.cpu_of_node) {
		dev_err(&pdev->dev,
			"Property 'rockchip,i2s-controller' missing or invalid\n");
		return -EINVAL;
	}

	rk_dailink.platform_of_node = rk_dailink.cpu_of_node;

	ret = snd_soc_of_parse_card_name(card, "rockchip,card-name");
	if (ret) {
		dev_err(&pdev->dev,
			"Soc parse card name failed %d\n", ret);
		return ret;
	}

	ret = devm_snd_soc_register_card(&pdev->dev, card);
	if (ret) {
		dev_err(&pdev->dev,
			"Soc register card failed %d\n", ret);
		return ret;
	}

	return ret;
}

static const struct of_device_id rockchip_es8316_of_match[] = {
	{ .compatible = "rockchip,rockchip-es8316", },
	{},
};

MODULE_DEVICE_TABLE(of, rockchip_es8316_of_match);

static struct platform_driver snd_rk_es8316_driver = {
	.probe = snd_rk_es8316_probe,
	.driver = {
		.name = DRV_NAME,
		.pm = &snd_soc_pm_ops,
		.of_match_table = rockchip_es8316_of_match,
	},
};

module_platform_driver(snd_rk_es8316_driver);

MODULE_AUTHOR("Xing Zheng <zhengxing@rock-chips.com>");
MODULE_DESCRIPTION("Rockchip es8316 machine ASoC driver");
MODULE_LICENSE("GPL v2");
MODULE_ALIAS("platform:" DRV_NAME);
